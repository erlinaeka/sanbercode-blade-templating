@extends('../layouts.master')
@section('titlepage','Tambah Cast')
@section('title','Cast')
@section('titlepart','Tambah Cast')
@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan umur">
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for=bio">Bio</label>
        <textarea class="form-control" id=bio" name="bio" placeholder="Masukkan Bio" rows="3"></textarea>
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type=" submit" class="btn btn-primary">Simpan</button>
    <a class="btn btn-danger" href="/cast" role="button">Kembali</a>

</form>
@endsection
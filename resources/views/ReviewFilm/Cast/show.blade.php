@extends('../layouts.master')
@section('titlepage','Detail Cast')
@section('title','Cast')
@section('titlepart','Detail Cast')
@section('content')
<p class="font-weight-bold">Nama Cast : {{$cast->nama}} </p>
<p class="font-weight-normal">Umur Cast : {{$cast->umur}} </p>
<p class="font-weight-normal">Bio Cast : {{$cast->bio}}</p>
<a class="btn btn-primary" href="/cast" role="button">Kembali</a>

@endsection
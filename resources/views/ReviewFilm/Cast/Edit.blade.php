@extends('../layouts.master')
@section('titlepage','Edit Cast')
@section('title','Cast')
@section('titlepart','Edit Cast')
@section('content')
<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{$cast->nama}}"
            placeholder="Masukkan nama">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class=" form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" id="umur" value="{{$cast->umur}}"
            placeholder="Masukkan umur">
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for=bio">Bio</label>
        <textarea class="form-control" id=bio" name="bio" placeholder="Masukkan Bio" rows="3">{{$cast->bio}}</textarea>
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type=" submit" class="btn btn-primary">Update</button>
    <a class="btn btn-danger" href="/cast" role="button">Kembali</a>

</form>
@endsection
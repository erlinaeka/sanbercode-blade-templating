@extends('../layouts.master')
@section('titlepage','Cast')
@section('title','Cast')
@section('titlepart','Data Cast')
@push('script')
<script src="{{ asset('template/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css" />

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.js"></script>
@endpush
@section('content')
<a class="btn btn-primary" href="/cast/create" role="button" style="margin-bottom: 20px;">Tambah</a>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($casts as $cast)
        <tr>
            <td>{{ $loop->iteration}}</td>
            <td>{{ $cast->nama }}</td>
            <td>{{ $cast->umur }}</td>
            <td>{{ $cast->bio }}</td>
            <td class="text-center"><a href="cast/{{ $cast->id }}/edit" class="btn btn-primary btn-sm"><i
                        class="fas fa-edit"></i></a>
                <a href="/cast/{{ $cast->id }}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                <form action="/cast/{{ $cast->id }}" method="POST" class="d-inline"
                    onsubmit="return confirm('Yakin Hapus Data?')">
                    @method('DELETE')
                    @csrf
                    <button class=" btn btn-danger btn-sm">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
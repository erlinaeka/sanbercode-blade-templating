<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('cast')->get();
        return view('ReviewFilm.Cast.Index', compact('casts'));
    }
    public function create()
    {
        return view('ReviewFilm.Cast.Create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast|max:45',
            'umur' => 'required|max:11',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);
        return redirect('/cast');
    }
    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('ReviewFilm.Cast.Show', compact('cast'));
    }
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('ReviewFilm.Cast.edit', compact('cast'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast|max:45',
            'umur' => 'required|max:11',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"],
            ]);
        return redirect('/cast');
    }
    public function destroy($id)
    {
        $cast = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
    //asasasaasasa
}
